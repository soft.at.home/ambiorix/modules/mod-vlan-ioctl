/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>

#include <sys/ioctl.h>
#include <errno.h>
#include <net/if.h>
#include <linux/sockios.h>
#include <linux/if_vlan.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include <amxm/amxm.h>

#include "mod_vlan_ioctl.h"

static int vlanfd = -1;

static int set_enabled(const char* name, bool enable) {
    int rv = -1;
    struct ifreq ifreq;
    strncpy(ifreq.ifr_name, name, IF_NAMESIZE);
    ifreq.ifr_name[IF_NAMESIZE - 1] = '\0';
    SAH_TRACEZ_INFO(ME, "Set vlan \"%s\" %s", name, enable ? "up" : "down");
    if(ioctl(vlanfd, SIOCGIFFLAGS, &ifreq) == -1) {
        SAH_TRACEZ_ERROR(ME, "Get flags of vlan \"%s\" failed: %d (%s)", name, errno, strerror(errno));
        rv = 2;
        goto exit;
    }

    if(enable) {
        ifreq.ifr_flags |= IFF_UP;
    } else {
        ifreq.ifr_flags &= ~IFF_UP;
    }

    if(ioctl(vlanfd, SIOCSIFFLAGS, &ifreq) == -1) {
        SAH_TRACEZ_ERROR(ME, "Set flags of vlan \"%s\" failed: %d (%s)", name, errno, strerror(errno));
        rv = 3;
        goto exit;
    }
    rv = 0;

exit:
    return rv;
}

static int set_name(amxc_var_t* ret, const char* name, const char* llintf, int32_t vlanid) {
    int rv = -1;
    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    struct ifreq ifreq;
    snprintf(ifreq.ifr_name, IF_NAMESIZE, "%s.%d", llintf, vlanid);
    if((name != NULL) && (*name != 0)) {
        snprintf(ifreq.ifr_newname, IF_NAMESIZE, "%s", name);
        amxc_var_add_key(cstring_t, ret, "vlanport_name", ifreq.ifr_newname);
        if(ioctl(vlanfd, SIOCSIFNAME, &ifreq) == -1) {
            SAH_TRACEZ_ERROR(ME, "failed to rename vlan (name=%s.%d newname=%s): %d(%s)", llintf, vlanid, name, errno, strerror(errno));
            rv = 3;
            goto exit;
        }
    } else {
        amxc_var_add_key(cstring_t, ret, "vlanport_name", ifreq.ifr_name);
        SAH_TRACEZ_INFO(ME, "no user friendly name set, using the default name: %s", ifreq.ifr_name);
    }
    rv = 0;

exit:
    return rv;
}

static int vlan_setprio(amxc_var_t* args, const char* name) {
    int rv = 2;
    int i = 0;
    uint32_t vlanpriority = GETP_UINT32(args, "egress_priority");

    SAH_TRACEZ_INFO(ME, "set vlan priority (name=%s vlanpriority=%d)", name, vlanpriority);

    struct vlan_ioctl_args ioctl_args;
    ioctl_args.cmd = SET_VLAN_EGRESS_PRIORITY_CMD;
    snprintf(ioctl_args.device1, sizeof(ioctl_args.device1), "%s", name);

    ioctl_args.u.skb_priority = 0;
    ioctl_args.vlan_qos = vlanpriority;
    if(ioctl(vlanfd, SIOCSIFVLAN, &ioctl_args) == -1) {
        SAH_TRACE_ERROR("Failed to set vlan %s egress priority(skb_priority=%d vlan_priority=0) failed: %d(%s)",
                        name, ioctl_args.vlan_qos, errno, strerror(errno));
        goto exit;
    }

    for(i = 1; i < 16; i++) {
        ioctl_args.u.skb_priority = i;
        ioctl_args.vlan_qos = (i % 8);
        if(ioctl(vlanfd, SIOCSIFVLAN, &ioctl_args) == -1) {
            SAH_TRACE_ERROR("Failed to set vlan %s egress priority(skb_priority=%d vlan_priority=%d) failed: %d(%s)",
                            name, i, ioctl_args.vlan_qos, errno, strerror(errno));
            goto exit;
        }
    }
    rv = 0;

exit:
    return rv;
}

static int create_vlan(UNUSED const char* function_name,
                       amxc_var_t* args,
                       UNUSED amxc_var_t* ret) {
    int rv = -1;
    const char* llintf = GETP_CHAR(args, "port_name");
    const char* name = GETP_CHAR(args, "vlanport_name");
    int32_t vlanid = GETP_INT32(args, "vlanid");

    //name can be empty, a default name will be used
    when_str_empty_trace(llintf, exit, ERROR, "no lowerlayer interface, no port configured");
    when_false_trace(vlanid > 0, exit, ERROR, "VLANID needs to be greater than 0");

    SAH_TRACEZ_INFO(ME, "create vlan(name=%s llintf=%s vlanid=%d)", name, llintf, vlanid);

    struct vlan_ioctl_args ioctl_args;
    // set VLAN NAME type so we are sure what the name will be
    ioctl_args.cmd = SET_VLAN_NAME_TYPE_CMD;
    ioctl_args.u.name_type = VLAN_NAME_TYPE_RAW_PLUS_VID_NO_PAD;
    if(ioctl(vlanfd, SIOCSIFVLAN, &ioctl_args) == -1) {
        SAH_TRACEZ_ERROR(ME, "failed to set name type, %d(%s)", errno, strerror(errno));
    }

    // add vlan
    ioctl_args.cmd = ADD_VLAN_CMD;
    snprintf(ioctl_args.device1, sizeof(ioctl_args.device1), "%s", llintf);
    ioctl_args.u.VID = vlanid;
    if(ioctl(vlanfd, SIOCSIFVLAN, &ioctl_args) == -1) {
        SAH_TRACEZ_ERROR(ME, "failed to add vlan (name=%s vlanid=%d): %d(%s)", llintf, vlanid, errno, strerror(errno));
        rv = 2;
        goto exit;
    }

    rv = set_name(ret, name, llintf, vlanid);
    when_failed(rv, exit);
    name = GETP_CHAR(ret, "0");

    ioctl_args.cmd = SET_VLAN_FLAG_CMD;
    snprintf(ioctl_args.device1, sizeof(ioctl_args.device1), "%s", name);
    ioctl_args.u.flag = VLAN_FLAG_REORDER_HDR;
    ioctl_args.vlan_qos = 1;
    if(ioctl(vlanfd, SIOCSIFVLAN, &ioctl_args) == -1) {
        SAH_TRACEZ_ERROR(ME, "failed to set vlan flags (name=%s flag=%d value=%d) failed: %d(%s)", name, VLAN_FLAG_REORDER_HDR, 1, errno, strerror(errno));
        rv = 4;
        goto exit;
    }

    rv = vlan_setprio(args, name);
    when_failed(rv, exit);

    rv = set_enabled(name, true);
    when_failed(rv, exit);

    SAH_TRACEZ_WARNING(ME, "Successfully created vlan %s on llintf %s with id %d", name, llintf, vlanid);

exit:
    return rv;
}


static int destroy_vlan(UNUSED const char* function_name,
                        amxc_var_t* args,
                        UNUSED amxc_var_t* ret) {
    int rv = -1;
    const char* name = GETP_CHAR(args, "vlanport_name");
    struct vlan_ioctl_args ioctl_args;
    amxc_string_t default_vlan_name;
    amxc_string_init(&default_vlan_name, 0);

    if((name == NULL) || (*name == '\0')) {
        const char* port_name = GET_CHAR(args, "port_name");
        uint32_t vlanid = GET_UINT32(args, "vlanid");

        when_str_empty_trace(port_name, exit, ERROR, "No vlan or port name provided");
        when_null_trace(GET_ARG(args, "vlanid"), exit, ERROR, "No vlan name and no vlan ID provided");
        amxc_string_setf(&default_vlan_name, "%s.%d", port_name, vlanid);
        name = amxc_string_get(&default_vlan_name, 0);
    }
    when_str_empty_trace(name, exit, ERROR, "can not destroy vlan, no name given");
    SAH_TRACEZ_INFO(ME, "vlan_unregister(name=%s)", name);

    // set vlan down before actually destroying it
    set_enabled(name, false);

    ioctl_args.cmd = DEL_VLAN_CMD;
    snprintf(ioctl_args.device1, sizeof(ioctl_args.device1), "%s", name);
    if(ioctl(vlanfd, SIOCSIFVLAN, &ioctl_args) == -1) {
        SAH_TRACEZ_ERROR(ME, "failed to delete vlan (name=%s): %d(%s)", name, errno, strerror(errno));
    } else {
        SAH_TRACEZ_WARNING(ME, "Successfully destroyed vlan %s", name);
    }

    rv = 0;

exit:
    amxc_string_clean(&default_vlan_name);
    return rv;
}

static int set_vlan_prio(UNUSED const char* function_name,
                         amxc_var_t* args,
                         UNUSED amxc_var_t* ret) {
    int rv = -1;
    const char* name = GETP_CHAR(args, "vlanport_name");
    when_str_empty_trace(name, exit, ERROR, "can not set priority, no vlan name provided");
    rv = vlan_setprio(args, name);

exit:
    return rv;
}


static AMXM_CONSTRUCTOR vlan_start(void) {
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;

    amxm_module_register(&mod, so, MOD_VLAN_CTRL);
    amxm_module_add_function(mod, "create-vlan", create_vlan);
    amxm_module_add_function(mod, "destroy-vlan", destroy_vlan);
    amxm_module_add_function(mod, "set-vlan-priority", set_vlan_prio);

    vlanfd = socket(AF_INET, SOCK_STREAM, 0);

    return 0;
}

static AMXM_DESTRUCTOR vlan_stop(void) {

    close(vlanfd);
    return 0;
}
